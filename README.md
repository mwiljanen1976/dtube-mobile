# DTube Mobile

To run this build from scratch first install cordova with:

npm -i -g cordova

Next clone this gitlab project:

git clone git@gitlab.com:mwiljanen1976/dtube-mobile.git

Next remove the files in the dtube-mobile/www folder and you can clone this folder from dtube/production

cd dtube-mobile

git clone https://github.com/dtube/production.git www

Next the app needs the DTube logo so you can copy the one in the www folder

cp www/favicon.ico www/logo.png

Create an android project in this folder with cordova:

cordova platform add android

Build the app with:

cordova build android

after you build you should get an APK file in this folder:

platforms/android/app/build/outputs/apk/debug/app-debug.apk

You can sideload the APK file with a USB cable or copy it and install it with the phone filemanager.

adb install platforms/android/app/build/outputs/apk/debug/app-debug.apk

Or you can run the build-dtube-mobile.sh and it will build and install to your phone.
